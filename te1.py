import requests
from requests import exceptions
from urllib.request import urlopen
from bs4 import BeautifulSoup
import re
from wxpy import *
import  schedule
import  time
 
 
bot=Bot(cache_path=True) #登陆网页微信，并保存登陆状态
 
def sendblogmsg(content):
    #搜索自己的好友，注意中文字符前需要+u
 
    my_group = bot.groups().search('あつまる大連チーム')[0]
    my_group.send(content) #发送天气预报
 
def job():
    resp=urlopen('https://tenki.jp/forecast/3/16/4410/13103/')
    soup=BeautifulSoup(resp,'html.parser')
    h_temp=soup.find(class_="high-temp temp")
    l_temp=soup.find(class_="low-temp temp")
      #第一个包含class="tem"的p标签即为存放今天天气数据的标签
    try:
        # temperatureHigh=h_temp.span.string  #有时候这个最高温度是不显示的，此时利用第二天的最高温度代替。
        temperatureHigh=h_temp.span.string # value #有时候这个最高温度是不显示的，此时利用第二天的最高温度代替。
        temperatureHigh=temperatureHigh+h_temp.find(class_="unit").string #unit
    except AttributeError as e:
        temperatureHigh=h_temp.find_next(class_="high-temp temp").span.string  #获取第二天的最高温度代替
 
    temperatureLow=l_temp.span.string+l_temp.find(class_ ="unit").string
    weather=soup.find('p',class_="weather-telop").string #获取天气
    contents = '東京' + '\n' +  '最高温度:' + temperatureHigh +'\n' + '最低温度:' + temperatureLow + '\n' +  '天気:' + weather 
       # result3 = '最低温度:' + temperatureLow
    print('最高温度:' + temperatureHigh)
    print('最低温度:' + temperatureLow)
    print('天气:' + weather)
    sendblogmsg(contents)
#定时
schedule.every().day.at("18:04").do(job) #规定每天12：30执行job()函数
while True:
    schedule.run_pending()#确保schedule一直运行
    time.sleep(1)
bot.join() #保证上述代码持续运行

